package View;

/**
 * Created by nikita on 26.05.2017.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Controller.*;
import View.Table;

public class Menu {
    JFrame frame;
    Controller controller;
    public Menu (){
        Dimension frameDim = new Dimension(800, 600);
        Dimension buttDim = new Dimension(350, 100);
        JPanel menuPanel = new JPanel(null);

        Font menuFont = new Font("Verdana", Font.PLAIN, 11);

        frame = new JFrame();
        frame.setSize(frameDim);
        frame.setTitle("Shoot The Cowboy!");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        //Добавление JMenuBar

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        fileMenu.setFont(menuFont);
        JMenuItem exitItem = new JMenuItem("exit");
        exitItem.setFont(menuFont);
        fileMenu.add(exitItem);
        menuBar.add(fileMenu);
        frame.setJMenuBar(menuBar);
        exitItem.addActionListener(new ExitActionListener());
        //


        //frame.getContentPane().setBackground(Color.black);

        Font font = new Font("e", Font.BOLD, 30);
        JButton start = new JButton("Start");
        start.setFont(font);
        JButton about = new JButton("Scores");
        about.setFont(font);
        JButton exit = new JButton("Exit");
        exit.setFont(font);
        JButton titlePic = new JButton();
        JButton cactusPic = new JButton();
        JButton tumbleweedPic = new JButton();

        start.setBackground(Color.black);
        about.setBackground(Color.black);
        exit.setBackground(Color.black);
        start.setForeground(Color.yellow);
        about.setForeground(Color.yellow);
        exit.setForeground(Color.yellow);

        start.setBorder(null);
        about.setBorder(null);
        exit.setBorder(null);
        titlePic.setBorder(null);
        cactusPic.setBorder(null);
        tumbleweedPic.setBorder(null);

        start.setSize(buttDim);
        about.setSize(buttDim);
        exit.setSize(buttDim);

        start.setLocation(225,250);
        about.setLocation(225, 350);
        exit.setLocation(225, 450);

        start.setFocusPainted(false);
        about.setFocusPainted(false);
        exit.setFocusPainted(false);

        titlePic.setSize(700,200);
        cactusPic.setSize(225,409);
        tumbleweedPic.setSize(225, 225);
        titlePic.setLocation(50,0);
        cactusPic.setLocation(0,200);
        tumbleweedPic.setLocation(575,300);

        Table.setCell(titlePic, "src/View/title.png");
        Table.setCell(cactusPic, "src/View/cactus.png");
        Table.setCell(tumbleweedPic, "src/View/Tumbleweed.png");

        frame.setContentPane(menuPanel);
        menuPanel.add(start);
        menuPanel.add(about);
        menuPanel.add(exit);
        menuPanel.add(titlePic);
        menuPanel.add(cactusPic);
        menuPanel.add(tumbleweedPic);
        menuPanel.setBackground(Color.black);
        menuPanel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/View/cursor-clipart2.png").getImage(),
                      new Point(0, 0), "custom cursor"));
        start.addActionListener(new StartActionListener());
        exit.addActionListener(new ExitActionListener());
        frame.setVisible(true);
    }

    public class StartActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
                Container menu = frame.getContentPane();
                menu.setVisible(false);
                controller = new Controller();
                controller.initGame(frame, menu);
        }
    }

    public class ExitActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.exit(0);
        }
    }
}
