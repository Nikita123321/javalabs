package commands;

import java.util.Map;
import java.util.Stack;

import lab2.CalcException;
import lab2.Cmd;

public class Add implements Cmd{
	public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
		if (stack.size() < 2) {
			CalcException e = new CalcException ("Too few elements in stack.");
			throw e;
		} else {
			stack.push(stack.pop() + stack.pop());
		}
	}
}
