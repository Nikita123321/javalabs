package View;

/**
 * Created by nikita on 26.05.2017.
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import Controller.*;

public class Menu {
    JFrame frame;
    Controller controller;
    public Menu (){
        Dimension frameDim = new Dimension(800, 600);
        Dimension buttDim = new Dimension(80, 60);
        JPanel menuPanel = new JPanel(new GridBagLayout());

        frame = new JFrame();
        frame.setSize(frameDim);
        frame.setTitle("Shoot The Cowboy!");
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);

        //frame.getContentPane().setBackground(Color.black);

        JButton start = new JButton("Start");
        JButton about = new JButton("About");
        JButton exit = new JButton("Exit");



        start.setBackground(Color.black);
        about.setBackground(Color.black);
        exit.setBackground(Color.black);
        start.setForeground(Color.white);
        about.setForeground(Color.white);
        exit.setForeground(Color.white);

        start.setBorderPainted(false);
        about.setBorderPainted(false);
        exit.setBorderPainted(false);

        start.setSize(buttDim);
        frame.setContentPane(menuPanel);
        menuPanel.add(start);
        menuPanel.add(about);
        menuPanel.add(exit);

        menuPanel.setBackground(Color.black);
        menuPanel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/View/cursor-clipart2.png").getImage(),
                      new Point(0, 0), "custom cursor"));
        start.addActionListener(new StartActionListener());
        frame.setVisible(true);
    }

    public class StartActionListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
                frame.getContentPane().setVisible(false);
                controller = new Controller();
                controller.initGame(frame);

        }
    }
}
