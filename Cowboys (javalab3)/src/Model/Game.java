package Model;

/**
 * Created by nikita on 26.05.2017.
 */

import View.*;
import javax.swing.*;
import Controller.Controller;
import java.util.concurrent.ThreadLocalRandom;


public class Game {

        Table table;
        Controller controller;

        public void isGotShooted(boolean flag)
        {
                if(flag == true)
                {
                        table.reduceHealthPoints();
                        table.changeHealth();
                }
        }

        public void startGame(JFrame frame, Controller contr)
        {
                controller = contr;
                table = new Table(frame, controller);
//                int randomNum = ThreadLocalRandom.current().nextInt(0, 8 + 1);
//                controller.launchBandit(table, randomNum, 300);
                WorkThread workThread = new WorkThread();
                workThread.setParametrs(table, 900);
                workThread.start();

        }
}
