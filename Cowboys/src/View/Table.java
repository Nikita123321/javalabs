package View;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import Controller.Controller;


/**
 * Created by nikita on 27.05.2017.
 */
    public class Table {
        int healthPoints = 3;
        int scorePoints = 0;
        JFrame frame = null;
        JFrame gameOver;
        JButton[] windows = new JButton[9];
        JButton[] hp = new JButton[3];
        JButton score;
        Controller controller;
        Container menu;

        public void setNullCondition()
        {
            scorePoints = 0;
            score.setText("SCORE: " + scorePoints);
            score.revalidate();
            score.repaint();
            healthPoints = 3;
            for (int i = 0; i < 3; i++)
            {
                setCell(hp[i], "src/View/heart.png");
                hp[i].revalidate();
                hp[i].repaint();
            }
        }

        public void setMenu(Container menu)
        {
            this.menu = menu;
        }

        public Controller getController()
        {
            return controller;
        }

        public int getHealthPoints()
        {return healthPoints;}

        public void reduceHealthPoints()
        {
            healthPoints--;
        }

        public JButton[] getWindows()
        {
            return windows;
        }

        public Table(JFrame gotFrame, Controller contr)
        {
            controller = contr;
            frame = gotFrame;
            drawTable(frame);
        }

        static public void setCell(JButton cell, String fileName)
        {
            ImageIcon image = new ImageIcon(fileName);
            cell.setIcon(image);
        }
        public void drawTable(JFrame frame)
        {
            JPanel mainPanel = new JPanel(null);
            JPanel table = new JPanel(new GridLayout(3, 3));
            JPanel health = new JPanel(new GridLayout(3,1));


            frame.setContentPane(mainPanel);
            frame.getContentPane().setVisible(true);

            mainPanel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/View/aim.png").getImage(),
                    new Point(0, 0), "custom cursor"));
            table.setSize(600,450);
            table.setLocation(100,75);
            mainPanel.add(table);

            health.setSize(100,210);
            health.setLocation(0,195);
            mainPanel.add(health);
            mainPanel.setBackground(Color.black);

            score = new JButton("SCORE: " + scorePoints);
            score.setBackground(Color.black);
            score.setForeground(Color.yellow);
            score.setSize(800, 75);
            score.setLocation(0,525);
            score.setBorder(null);
            mainPanel.add(score);


            for(int i = 0; i < 9; i++) {
                windows[i] = new JButton();
                setCell(windows[i], "src/View/doors1.png");
                table.add(windows[i]);
            }

            for(int i =0; i < 3; i++)
            {
                hp[i] = new JButton();
                setCell(hp[i], "src/View/heart.png");
                hp[i].setBorder(null);
                hp[i].setBackground(Color.black);
                health.add(hp[i]);
            }
        }

        public void changeHealth()
        {
            if(healthPoints >= 0) {
                hp[2 - healthPoints].setIcon(null);
                hp[2 - healthPoints].revalidate();
                hp[2 - healthPoints].repaint();
            }
        }

        public  void increaseScore()
        {
            scorePoints++;
            score.setText("SCORE: " + scorePoints);
            score.revalidate();
            score.repaint();
        }

        public void gameOver ()
        {
            frame.setEnabled(false);
            gameOver = new JFrame();
            JPanel goPanel = new JPanel(null);

            gameOver.setContentPane(goPanel);
            gameOver.setBackground(Color.black);
            gameOver.setSize(400, 250);
            gameOver.setLocationRelativeTo(null);
            gameOver.setUndecorated(true);
            gameOver.setResizable(false);
            gameOver.setVisible(true);
            gameOver.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            goPanel.setBackground(Color.black);
            gameOver.setEnabled(true);

            goPanel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/View/cursor-clipart2.png").getImage(),
                    new Point(0, 0), "custom cursor"));

            JButton scr = new JButton("Game over!. Your score is " + scorePoints + " . Try again or exit?");
            JButton exit = new JButton("EXIT");
            JButton tryAgain = new JButton("TRY AGAIN");
            JButton pic = new JButton();

            scr.setBorder(null);
            exit.setBorder(null);
            tryAgain.setBorder(null);
            pic.setBorder(null);

            exit.setBackground(Color.black);
            tryAgain.setBackground(Color.black);

            exit.setForeground(Color.yellow);
            tryAgain.setForeground(Color.yellow);

            scr.setBackground(Color.black);
            scr.setForeground(Color.yellow);

            scr.setSize(350,100);
            scr.setLocation(25,0);
            goPanel.add(scr);
            scr.setFocusPainted(false);

            exit.addActionListener(new ExitActionListener());
            exit.setSize(100, 75);
            exit.setLocation(25,200);

            goPanel.add(exit);
            exit.setFocusPainted(false);

            tryAgain.addActionListener(new TryAgainActionListener());
            tryAgain.setSize(100, 75);
            tryAgain.setLocation(275, 200);
            goPanel.add(tryAgain);
            tryAgain.setFocusPainted(false);

            pic.setSize(150,100);
            pic.setLocation(125,101);
            setCell(pic, "src/View/gameOver.png");
            goPanel.add(pic);

            pic.revalidate();
            pic.repaint();

            exit.revalidate();
            exit.repaint();

            tryAgain.revalidate();
            tryAgain.repaint();

            scr.revalidate();
            scr.repaint();
        }

        public class ExitActionListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                frame.setEnabled(true);
                gameOver.dispose();
                frame.getContentPane().setVisible(false);
                frame.setContentPane(menu);
                menu.setVisible(true);
            }
        }

        public class TryAgainActionListener implements ActionListener {
            @Override
            public void actionPerformed(ActionEvent e)
            {

                frame.setEnabled(true);
                gameOver.dispose();
                controller.getGame().startGame();
            }
        }
}
