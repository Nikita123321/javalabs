package commands;

import java.util.Map;
import java.util.Stack;

import lab2.CalcException;
import lab2.Cmd;

public class Print implements Cmd{
	public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
		if (stack.size() == 0) {
			CalcException e = new CalcException ("Empty stack!");
			throw e;
		} else {
			System.out.println(stack.peek());
		}
	}
}

