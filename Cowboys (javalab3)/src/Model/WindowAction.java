package Model;

/**
 * Created by nikita on 27.05.2017.
 */
import Controller.Controller;
import View.Table;

import javax.swing.*;
import java.lang.Thread;
import java.util.concurrent.TimeUnit;


public class WindowAction extends Thread  implements Runnable {
    Table table;
    boolean ifRemoved = false;
    int button;
    int delay;
    Controller.WindowActionListener al;
    ImageIcon[] images = new ImageIcon[5];

    private void setImages()
    {
        images[0] = new ImageIcon("src/View/1.png");
        images[1] = new ImageIcon("src/View/2.png");
        images[2] = new ImageIcon("src/View/3.png");
        images[3] = new ImageIcon("src/View/4.png");
        images[4] = new ImageIcon("src/View/Doors1.png");
    }

    public void setParametrs(Table gotTable, int but, int del, Controller.WindowActionListener al)
    {
        this.al = al;
        table = gotTable;
        button = but;
        delay = del;
    }

    public boolean getifRemoved()
    {
        return ifRemoved;
    }

    public void Remove()
    {
        ifRemoved = true;
    }
    public void run()
    {
        JButton window = table.getWindows()[button];
        setImages();

            for (int i = 0; i < 5; i++) {
                try {
                    TimeUnit.MILLISECONDS.sleep(delay);
                } catch (InterruptedException e) {

                }
                if (ifRemoved) {
                    return;
                }
                window.setIcon(images[i]);
                window.revalidate();
                window.repaint();
            }

        table.getWindows()[button].removeActionListener(al);
        table.getController().shootResult(false);

    }
}
