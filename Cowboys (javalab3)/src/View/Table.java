package View;

import javax.swing.*;
import java.awt.*;
import Controller.Controller;


/**
 * Created by nikita on 27.05.2017.
 */
public class Table {
        JFrame frame = null;
        JButton[] windows = new JButton[9];
        JButton[] hp = new JButton[3];
        int healthPoints = 3;
        Controller controller;
        JButton score;
        int scorePoints = 0;

        public Controller getController()
        {
            return controller;
        }

        public int getHealthPoints()
        {return healthPoints;}

        public void reduceHealthPoints()
        {
            healthPoints--;
        }

        public JButton[] getWindows()
        {
            return windows;
        }

        public Table(JFrame gotFrame, Controller contr)
        {
            controller = contr;
            frame = gotFrame;
            drawTable(frame);
        }

        static public void setCell(JButton cell, String fileName)
        {
            ImageIcon image = new ImageIcon(fileName);
            cell.setIcon(image);
        }
        public void drawTable(JFrame frame)
        {
            JPanel mainPanel = new JPanel(null);
            JPanel table = new JPanel(new GridLayout(3, 3));
            JPanel health = new JPanel(new GridLayout(3,1));


            frame.setContentPane(mainPanel);
            frame.getContentPane().setVisible(true);

            mainPanel.setCursor(Toolkit.getDefaultToolkit().createCustomCursor(new ImageIcon("src/View/aim.png").getImage(),
                    new Point(0, 0), "custom cursor"));
            table.setSize(600,450);
            table.setLocation(100,75);
            mainPanel.add(table);

            health.setSize(100,210);
            health.setLocation(0,195);
            mainPanel.add(health);
            mainPanel.setBackground(Color.black);

            score = new JButton("SCORE: " + scorePoints);
            score.setBackground(Color.black);
            score.setForeground(Color.yellow);
            score.setSize(800, 75);
            score.setLocation(0,525);
            score.setBorder(null);
            mainPanel.add(score);


            for(int i = 0; i < 9; i++) {
                windows[i] = new JButton();
                setCell(windows[i], "src/View/doors1.png");
                table.add(windows[i]);
            }

            for(int i =0; i < 3; i++)
            {
                hp[i] = new JButton();
                setCell(hp[i], "src/View/heart.png");
                hp[i].setBorder(null);
                hp[i].setBackground(Color.black);
                health.add(hp[i]);
            }
        }

        public void changeHealth()
        {
            hp[2-healthPoints].setIcon(null);
            hp[2-healthPoints].revalidate();
            hp[2-healthPoints].repaint();
        }

        public  void increaseScore()
        {
            scorePoints++;
            score.setText("SCORE: " + scorePoints);
            score.revalidate();
            score.repaint();
        }

        public void gameOver ()
        {
            frame.setEnabled(false);
            JFrame gameOver = new JFrame();
            JPanel goPanel = new JPanel(null);

            gameOver.setContentPane(goPanel);
            gameOver.setBackground(Color.black);
            gameOver.setSize(400, 250);
            gameOver.setLocationRelativeTo(null);
            gameOver.setUndecorated(true);
            gameOver.setResizable(false);
            gameOver.setVisible(true);
            goPanel.setBackground(Color.black);
            gameOver.setEnabled(true);

        }
}
