package Controller;

/**
 * Created by nikita on 27.05.2017.
 */

import javax.swing.*;
import Model.*;
import Model.WindowAction;
import View.Table;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controller {

        Game game;

        public Game getGame()
        {
            return game;
        }

        public void shootResult (boolean isKilled)
        {
            game.isGotShooted(!isKilled);
        }

        public void initGame(JFrame frame, Container menu)
        {
            game = new Game();
            game.setParametrs(frame, this, menu);
            game.startGame();
        }

        public void launchBandit(Table table, int num, int delay)
        {
            WindowAction launch = new WindowAction();
            WindowActionListener listener = new WindowActionListener();
            listener.init(launch, table, num);
            JButton window = table.getWindows()[num];
            window.addActionListener(listener);


            launch.setParametrs(table, num, delay, listener);
            launch.start();

        }

        public class WindowActionListener implements ActionListener
        {
            WindowAction thread;
            Table table;
            int button;
            void init(WindowAction thread, Table table, int button) {
                this.table = table;
                this.button = button;
                this.thread = thread;
            }
            @Override
            public void actionPerformed(ActionEvent e) {
                thread.Remove();
                table.setCell(table.getWindows()[button], "src/View/Doors1.png");
                table.getWindows()[button].removeActionListener(this);
                table.increaseScore();
                shootResult(true);
            }
        }
}
