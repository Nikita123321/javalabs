package commands;

import java.util.Map;
import java.util.Stack;

import lab2.CalcException;
import lab2.Cmd;

public class Div implements Cmd{
	public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
		if (stack.size() < 2) {
			CalcException e1 = new CalcException ("Too few elements in stack.");
			throw e1;
		} else {
			stack.push( 1 / stack.pop() * stack.pop());
		}
	}
}
