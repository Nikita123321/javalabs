package lab2;

public class CalcException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5578022212249692032L;

	public CalcException (String message) {
		super(message);
	}
		
}
