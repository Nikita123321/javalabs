package lab2;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class Main {

	public static void main(String[] args) {
		InputStream stream = null;
		
		if (args.length > 0) {
			try {
				stream = new FileInputStream(args[0]);
			} catch (FileNotFoundException e) {
				System.out.println("File " + args[0] + " not found!");
			}
		} else {
			stream = System.in;
		}
		Calculator calc = new Calculator(stream);
		try {
			calc.execute();
		} catch (IOException e) {
			System.out.println("Cannot open commands");
		}
	}

}
