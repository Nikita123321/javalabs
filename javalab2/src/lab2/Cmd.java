package lab2;

import java.util.Map;
import java.util.Stack;
import lab2.CalcException;

public interface Cmd {
	void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException;
}
