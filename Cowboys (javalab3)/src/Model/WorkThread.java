package Model;

/**
 * Created by nikita on 27.05.2017.
*/
import Controller.Controller;
import View.Table;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

public class WorkThread extends Thread  implements Runnable{

    Table table;
    int delay;

    public void setParametrs(Table gotTable,  int del)
    {
        table = gotTable;
        delay = del;
    }

    public void run()
    {

        while (true) {
            if(table.getHealthPoints() == 0)
            {   table.gameOver();
                break;
            }
            try
            {
                int randomNum = ThreadLocalRandom.current().nextInt(0, 8 + 1);

                table.getController().launchBandit(table, randomNum, 300);
                System.out.println("go sleep");
                Thread.sleep(900);
                System.out.println("alive");
            }
            catch (InterruptedException e){

            }
        }

    }
}
