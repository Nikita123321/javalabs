package commands;

import java.util.Map;
import java.util.Stack;
import lab2.CalcException;
import lab2.Cmd;

public class Push implements Cmd{
	public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
		try {
			stack.push(Double.valueOf(argv[1]));
		} catch (NumberFormatException e) {
			if (param.containsKey(argv[1])) {
				stack.push(param.get(argv[1]));
			} else {
				CalcException ex = new CalcException("No parameter with name " + argv[1]);
				throw ex;
			}
		} catch (ArrayIndexOutOfBoundsException e) {
			CalcException ex = new CalcException("Not enough arguments!");
			throw ex;
		}
	}
}

