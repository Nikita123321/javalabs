package commands;

import java.util.EmptyStackException;
import java.util.Map;
import java.util.Stack;

import lab2.CalcException;
import lab2.Cmd;

public class Pop implements Cmd{
	public void exec(Stack<Double> stack, Map<String, Double> param, String[] argv) throws CalcException {
		try {
			System.out.println(stack.pop());
		} catch (EmptyStackException e) {
			CalcException e1 = new CalcException ("Empty stack!");
			throw e1;
		}
	}
}

