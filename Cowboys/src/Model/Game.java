package Model;

/**
 * Created by nikita on 26.05.2017.
 */

import View.*;
import javax.swing.*;
import Controller.Controller;

import java.awt.*;
import java.util.concurrent.ThreadLocalRandom;


public class Game {

        Table table;
        Controller controller;

        public void setParametrs(JFrame frame, Controller controller, Container menu)
        {
                this.controller = controller;
                table = new Table(frame, this.controller);
                table.setMenu(menu);

        }

        public Table getTable()
        {
                return table;
        }

        public void isGotShooted(boolean flag)
        {
                if(flag == true)
                {
                        table.reduceHealthPoints();
                        table.changeHealth();
                }
        }

        public void startGame()
        {
//                int randomNum = ThreadLocalRandom.current().nextInt(0, 8 + 1);
//                controller.launchBandit(table, randomNum, 300);
                table.setNullCondition();
                WorkThread workThread = new WorkThread();
                workThread.setParametrs(table, 900);
                workThread.start();

        }
}
