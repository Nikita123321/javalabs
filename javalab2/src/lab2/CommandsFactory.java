package lab2;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class CommandsFactory {
	private final Map<String, Class<?>> map = new HashMap<>();
	private static CommandsFactory ins;
	
	private CommandsFactory () throws IOException {
		Properties prop = new Properties();
		InputStream iStream = CommandsFactory.class.getResourceAsStream("commands.txt");
		if (iStream == null) {
			IOException e = new IOException();
			throw e;
		}
		prop.load(iStream);
		
		for (String key : prop.stringPropertyNames()) {
			try {
				Class<?> com = Class.forName(prop.getProperty(key));
				map.put(key, com);
			} catch (Exception e) {
				System.out.println("Error adding command " + key);
			}
		}
	}
	
	public Cmd getCommandByName (String name) {
		if(map.get(name) == null)
			return null;
		else
			try {
				Cmd ret = (Cmd) map.get(name).newInstance();
				return ret;
			} 
			catch (Exception e){
				System.out.println("Can't access command" + name);
				return null;
			}
	}
	
	public static CommandsFactory getInstance () throws IOException {
		if (ins == null) 
			ins = new CommandsFactory();
		return ins;
	
	}
}
